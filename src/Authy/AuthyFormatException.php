<?php

namespace Authy;

use Exception;

/**
 * Custom Authy API Exceptions
 *
 * PHP version 7
 *
 * @category Services
 * @package  Authy
 * @author   Angel Hidalgo <aghabrego@gmail.com>
 * @license  http://creativecommons.org/licenses/MIT/ MIT
 * @link     https://gitlab.com/grupoprodecasa/authy
 */
class AuthyFormatException extends Exception
{
}
