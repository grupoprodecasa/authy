<?php

namespace Authy;

use Authy\AuthyResponse;

/**
 * Token implementation. Extends from Authy_Response
 *
 * PHP version 7
 *
 * @category Services
 * @package  Authy
 * @author   Angel Hidalgo <aghabrego@gmail.com>
 * @license  http://creativecommons.org/licenses/MIT/ MIT
 * @link     https://gitlab.com/grupoprodecasa/authy
 */
class AuthyToken extends AuthyResponse
{

    /**
     * Check if the response was ok
     *
     * @return boolean
     */
    public function ok()
    {
        if (parent::ok()) {
            return $this->bodyvar('token') == 'is valid';
        }

        return false;
    }
}
