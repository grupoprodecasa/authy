<?php

namespace Authy;

use Authy\AuthyUser;
use Authy\AuthyToken;
use GuzzleHttp\Client;
use Authy\AuthyResponse;
use Authy\AuthyFormatException;

/**
 * ApiClient
 *
 * PHP version 7
 *
 * @category Services
 * @package  Authy
 * @author   Angel Hidalgo <aghabrego@gmail.com>
 * @license  http://creativecommons.org/licenses/MIT/ MIT
 * @link     https://gitlab.com/grupoprodecasa/authy
 */
class AuthyApi
{
    const VERSION = '0.0.1';

    /**
     * @var Client
     */
    protected $rest;

    /**
     * @var string
     */
    protected $api_url;

    /**
     * @var array
     */
    protected $default_options;

    /**
     * @param string $api_key
     * @param string $api_url
     * @param mixed $http_handler
     * @return void
     */
    public function __construct($api_key, $api_url = "https://api.authy.com", $http_handler = null)
    {
        $client_opts = [
            'base_uri' => "{$api_url}/",
            'headers' => ['User-Agent' => $this->__getUserAgent(), 'X-Authy-API-Key' => $api_key],
            'http_errors' => false,
        ];

        if ($http_handler != null) {
            $client_opts['handler'] = $http_handler;
        }

        $this->rest = new Client($client_opts);
        $this->api_url = $api_url;
        $this->default_options = ['curl' => [CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4]];
    }

    /**
     * Register a user.
     *
     * @param string $email
     * @param string $cellphone
     * @param int $country_code
     * @return AuthyUser
     */
    public function registerUser($email, $cellphone, $country_code = 1, $send_install_link = true)
    {
        $resp = $this->rest->post('protected/json/users/new', array_merge(
            $this->default_options,
            [
                'query' => [
                    'user' => [
                        "email" => $email,
                        "cellphone" => $cellphone,
                        "country_code" => $country_code,
                        "send_install_link_via_sms" => $send_install_link,
                    ]
                ]
            ]
        ));

        return new AuthyUser($resp);
    }

    /**
     * Verify a given token.
     *
     * @param string $authy_id User's id stored in your database
     * @param string $token The token entered by the user
     * @param array  $opts Array of options, for example: array("force" => "true")
     * @return AuthyResponse
     */
    public function verifyToken($authy_id, $token, $opts = [])
    {
        if (!array_key_exists("force", $opts)) {
            $opts["force"] = "true";
        } else {
            unset($opts["force"]);
        }

        $token = urlencode($token);
        $authy_id = urlencode($authy_id);
        $this->__validateVerify($token, $authy_id);

        $resp = $this->rest->get("protected/json/verify/{$token}/{$authy_id}", array_merge(
            $this->default_options,
            ['query' => $opts]
        ));

        return new AuthyToken($resp);
    }

    /**
     * Request a valid token via SMS.
     *
     * @param string $authy_id User's id stored in your database
     * @param array $opts Array of options, for example: array("force" => "true")
     * @return AuthyResponse
     */
    public function requestSms($authy_id, $opts = [])
    {
        $authy_id = urlencode($authy_id);

        $resp = $this->rest->get("protected/json/sms/{$authy_id}", array_merge(
            $this->default_options,
            ['query' => $opts]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Request a valid token via EMAIL.
     *
     * @param string $authy_id User's id stored in your database
     * @param array $opts Array of options, for example: array("force" => "true")
     * @return AuthyResponse
     */
    public function requesEmail($authy_id, $opts = [])
    {
        $authy_id = urlencode($authy_id);

        $resp = $this->rest->post("protected/json/email/{$authy_id}", array_merge(
            $this->default_options,
            ['query' => $opts]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Cellphone call, usually used with SMS Token issues or if no smartphone is available.
     * This function needs the app to be on Starter Plan (free) or higher.
     *
     * @param string $authy_id User's id stored in your database
     * @param array $opts Array of options, for example: array("force" => "true")
     * @return AuthyResponse
     */
    public function phoneCall($authy_id, $opts = [])
    {
        $authy_id = urlencode($authy_id);
        $resp = $this->rest->get("protected/json/call/{$authy_id}", array_merge(
            $this->default_options,
            ['query' => $opts]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Deletes an user.
     *
     * @param string $authy_id User's id stored in your database
     * @return AuthyResponse
     */
    public function deleteUser($authy_id)
    {
        $authy_id = urlencode($authy_id);
        $resp = $this->rest->post("protected/json/users/{$authy_id}/remove", $this->default_options);

        return new AuthyResponse($resp);
    }

    /**
     * Update user mail.
     *
     * @param string $authy_id User's id stored in your database
     * @param array $opts Array of options, for example: array("force" => "true")
     * @return AuthyResponse
     */
    public function updateEmailUser($authy_id, $opts = [])
    {
        $authy_id = urlencode($authy_id);

        $resp = $this->rest->post("protected/json/users/{$authy_id}/update", array_merge(
            $this->default_options,
            ['query' => $opts]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Gets user status.
     *
     * @param string $authy_id User's id stored in your database
     * @return AuthyResponse
     */
    public function userStatus($authy_id)
    {
        $authy_id = urlencode($authy_id);
        $resp = $this->rest->get("protected/json/users/{$authy_id}/status", $this->default_options);

        return new AuthyResponse($resp);
    }

    /**
     * Starts phone verification. (Sends token to user via sms or call).
     *
     * @param string $phone_number
     * @param string $country_code
     * @param string $via (sms or call)
     * @param int $code_length
     * @return AuthyResponse
     */
    public function phoneVerificationStart($phone_number, $country_code, $via = 'sms', $code_length = 4, $locale = null)
    {
        $query = [
            "phone_number" => $phone_number,
            "country_code" => $country_code,
            "via" => $via,
            "code_length" => $code_length
        ];

        if ($locale != null) {
            $query["locale"] = $locale;
        }

        $resp = $this->rest->post("protected/json/phones/verification/start", array_merge(
            $this->default_options,
            ['query' => $query]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Phone verification check. (Checks whether the token entered by the user is valid or not).
     *
     * @param string $phone_number
     * @param string $country_code
     * @param string $verification_code
     * @return AuthyResponse
     */
    public function phoneVerificationCheck($phone_number, $country_code, $verification_code)
    {
        $resp = $this->rest->get("protected/json/phones/verification/check", array_merge(
            $this->default_options,
            [
                'query' => [
                    "phone_number" => $phone_number,
                    "country_code" => $country_code,
                    "verification_code" => $verification_code
                ]
            ]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Phone information. (Checks whether the token entered by the user is valid or not).
     *
     * @param string $phone_number
     * @param string $country_code
     * @return AuthyResponse
     */
    public function phoneInfo($phone_number, $country_code)
    {
        $resp = $this->rest->get("protected/json/phones/info", array_merge(
            $this->default_options,
            [
                'query' => [
                    "phone_number" => $phone_number,
                    "country_code" => $country_code
                ]
            ]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Create a new approval request for a user
     *
     * @param string $authy_id User's id stored in your database
     * @param array $opts Array of options, for example: array("force" => "true")
     * @return AuthyResponse
     */
    public function createApprovalRequest($authy_id, $message, $opts = [])
    {
        $opts['message'] = $message;

        $authy_id = urlencode($authy_id);
        $resp = $this->rest->post("onetouch/json/users/{$authy_id}/approval_requests", array_merge(
            $this->default_options,
            ['query' => $opts]
        ));

        return new AuthyResponse($resp);
    }

    /**
     * Check the status of an approval request
     *
     * @param string $request_uuid The UUID of the approval request you want to check
     * @return AuthyResponse
     */
    public function getApprovalRequest($request_uuid)
    {
        $request_uuid = urlencode($request_uuid);
        $resp = $this->rest->get("onetouch/json/approval_requests/{$request_uuid}");

        return new AuthyResponse($resp);
    }

    /**
     * @return string
     */
    private function __getUserAgent()
    {
        return sprintf(
            'AuthyPHP/%s (%s-%s-%s; PHP %s)',
            AuthyApi::VERSION,
            php_uname('s'),
            php_uname('r'),
            php_uname('m'),
            phpversion()
        );
    }

    /**
     * @return void
     * @throws AuthyFormatException
     */
    private function __validateVerify($token, $authy_id)
    {
        $this->__validate_digit($token, "Invalid Token. Only digits accepted.");
        $this->__validate_digit($authy_id, "Invalid Authy id. Only digits accepted.");
        $length = strlen((string)$token);
        if ($length < 6 or $length > 10) {
            throw new AuthyFormatException("Invalid Token. Unexpected length.");
        }
    }

    /**
     * @return void
     * @throws AuthyFormatException
     */
    private function __validate_digit($var, $message)
    {
        if (!is_int($var) && !is_numeric($var)) {
            throw new AuthyFormatException($message);
        }
    }
}
