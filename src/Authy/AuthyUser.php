<?php

namespace Authy;

use Authy\AuthyResponse;

/**
 * User implementation. Extends from Authy_Response
 *
 * PHP version 7
 *
 * @category Services
 * @package  Authy
 * @author   Angel Hidalgo <aghabrego@gmail.com>
 * @license  http://creativecommons.org/licenses/MIT/ MIT
 * @link     https://gitlab.com/grupoprodecasa/authy
 */
class AuthyUser extends AuthyResponse
{
    /**
     * @param object $raw_response
     */
    public function __construct($raw_response)
    {
        $body = json_decode($raw_response->getBody());

        if (isset($body->user)) {
            // response is {user: {id: id}}
            $raw_response->body = $body->user;
        }

        parent::__construct($raw_response);
    }
}
