<?php
namespace App\Library\Services;

use GuzzleHttp\Client;

/**
 * ApiClient
 *
 * PHP version 7
 *
 * @category Services
 * @package  Authy
 * @author   Angel Hidalgo <aghabrego@gmail.com>
 * @license  http://creativecommons.org/licenses/MIT/ MIT
 * @link     https://gitlab.com/grupoprodecasa/authy
 */
class OneTouchService
{
    /**
     * @var string
     */
    protected $authyBaseUrl;

    /**
     * @param string $api_key
     * @param string $api_url
     * @return void
     */
    public function __construct(string $api_key, string $api_url = "https://api.authy.com")
    {
        /** @var string */
        $phpVersion = phpversion();

        $this->userAgent = "PhoneVericationPHPReg (PHP {$phpVersion})";
        $this->authyApiKey = $api_key;
        $this->authyBaseUrl = $api_url;
        $this->headers = ['User-Agent' => $this->userAgent];
        $this->client = new Client();
    }

    /**
     * @param string $path
     * @param string
     */
    private function getUrl(string $path)
    {
        return "{$this->authyBaseUrl}{$path}";
    }

    /**
     * @param array $requestData
     * @return mixed
     */
    public function createApprovalRequest(array $requestData)
    {
        $requestData['api_key'] = $this->authyApiKey;
        $path = "/onetouch/json/users/{$requestData['visible']['AuthyID']}/approval_requests";

        $response = $this->client->post($this->getUrl($path), [
            'headers' => $this->headers,
            'body' => $requestData
        ]);

        return $response->getBody()['approval_request']['uuid'];
    }

    /**
     * @param string $onetouch_uuid
     * @return mixed
     */
    public function oneTouchStatus(string $onetouch_uuid)
    {
        $data = [
            'api_key' => $this->authyApiKey
        ];
        $path = "/onetouch/json/approval_requests/{$onetouch_uuid}";

        $response = $this->client->post($this->getUrl($path), [
          'headers' => $this->headers,
          'body' => $data,
          'query' => ['api_key' => $this->authyApiKey]
        ]);

        return $response->getBody();
    }
}
