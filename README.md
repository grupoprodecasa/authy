# Weirdo Labs

> Weirdo Labs, Cliente PHP para Authy.

## Caracteristicas

- PHP ^8.0|^8.1
- Laravel 8

### Como utilizarlo

- `git clone git@gitlab.com:grupoprodecasa/authy.git`
- `cd authy`
- `composer install`
- `./vendor/bin/phpunit`

## Copyright

Ver [LICENSE](https://gitlab.com/grupoprodecasa/authy/blob/master/LICENSE) para mas detalles.
