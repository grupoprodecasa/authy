# Changelog

## 0.0.7 - 2023-04-18

- Agregando funcionalidad para actualizar email.
- Agregando funcionalidad para enviar verificación por email.
- Fix: Corrigiendo método de la petición al enviar el código por email.

## 0.0.6 - 2022-09-09

- Actualizando dependencias.
- Actualizando imagen.
- Optimizando composer.json.

## 0.0.5 - 2022-02-23

- Corrigiendo nombre y año en el archivo ```LICENSE```.

## 0.0.4 - 2022-02-12

- Update support for Laravel Framework v9.
- Increase minimum PHP version to 8.0 and above (tested with 8.0 and 8.1).
- Fix: Corrigiendo composer.
- Fix: Manteniendo soporte symfony 5.4 y 6.0.
- Fix: Agregando ````.phpunit.result.cache``` al .gitignore.
- Agregando archivo LICENSE.
- Actualizando ```README.md```.
- Fix: Corrigiendo versionamiento en el README.md de Laravel.
- Agregando archivo de configuración de job de gitlab ```.gitlab-ci.yml```.

## 0.0.3 - 2021-10-18

- Fix: Definiendo constantes.
- Fix: Eliminando constante en clase.

## 0.0.2 - 2021-07-12

- Agregando servicio ```OneTouchService```.

## 0.0.1 - 2021-07-11

- Proyecto inicia
